package net.netconomy.ts;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class ContactBook {

	private HashMap<String, ArrayList<Contact>> contactsMap;

	public ContactBook() {
		super();
		this.contactsMap = new HashMap<String, ArrayList<Contact>>() ;
	}

	public ContactBook(HashMap<String, ArrayList<Contact>> contactsMap) {
		super();
		this.contactsMap = contactsMap;
	}

	public ArrayList<Contact> getContacts() {
		ArrayList<Contact> allContacts = new ArrayList<Contact>();
		for (ArrayList<Contact> contacts : contactsMap.values()) {
			allContacts.addAll(contacts);
		}
		return allContacts;
	}

	public String getKeyForContact(Contact contact) {
		return contact.getFirstName() + "_" + contact.getLastName();
	}

	public ArrayList<Contact> getEntriesForContact(Contact contact) {
		return contactsMap.get(getKeyForContact(contact));

	}

	public void loadFromCsv(String path, String separator) throws ContactLoadException {
		HashMap<String, ArrayList<Contact>> csvContactsMap = new HashMap<String, ArrayList<Contact>>();

		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(path));
			String line;
			while ((line = br.readLine()) != null) {
				String[] contactAttributes = line.split(separator, -1);
				String firstName = contactAttributes[0];
				String lastName = contactAttributes[1];
				String mobileNumber = contactAttributes[2];
				String email = contactAttributes[3];
				Contact contact = new Contact(firstName, lastName, mobileNumber, email);

				// missing values are only allowed for either mobile number, or email
				if (mobileNumber.isEmpty() && email.isEmpty()) {
					throw new ContactLoadException(
							"Both mobile number and email address are missing for: " + firstName + " " + lastName);
				}

				String contactKey = getKeyForContact(contact);
				if (csvContactsMap.containsKey(contactKey)) {
					csvContactsMap.get(contactKey).add(contact);
				} else {
					ArrayList<Contact> entries = new ArrayList<Contact>();
					entries.add(contact);
					csvContactsMap.put(contactKey, entries);
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		contactsMap = new HashMap<String, ArrayList<Contact>>(csvContactsMap);

	}

	public void exportToCsv(String path, String separator) throws ContactExportException{
		BufferedWriter bw = null;
		FileWriter fw = null;
		try {

			fw = new FileWriter(path);
			bw = new BufferedWriter(fw);
			String content = "";
			for (ArrayList<Contact> contacts : contactsMap.values()) {
				for (Contact contact : contacts) {
					if (contact.getMobileNumber().isEmpty() && contact.getEmail().isEmpty()) {
						throw new ContactExportException("Both mobile number and email address are missing for: "
								+ contact.getFirstName() + " " + contact.getLastName());
					}

					content = content + contact.getFirstName() + separator + contact.getLastName() + separator
							+ contact.getMobileNumber() + separator + contact.getEmail() + separator
							+ System.lineSeparator();
				}
			}
			bw.write(content);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}