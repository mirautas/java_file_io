package net.netconomy.ts;

public class ContactLoadException extends ContactBookException {
	public ContactLoadException(String errorMessage) {
		super(errorMessage);
	}

}
