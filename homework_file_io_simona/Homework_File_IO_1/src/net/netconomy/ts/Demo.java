package net.netconomy.ts;

import java.util.ArrayList;

public class Demo {
	public static void main(String[] args) {
		ContactBook contactBook = new ContactBook();
		try {
			contactBook.loadFromCsv("C:\\Users\\smirauta\\eclipse-workspace\\Homework_File_IO\\input\\Contacts.txt", ";");
		} catch (ContactLoadException e) {
			e.printStackTrace();
		}		
		
		contactBook.exportToCsv("C:\\Users\\smirauta\\eclipse-workspace\\Homework_File_IO\\output\\ContactsOutput.txt", ";");
	}

}
