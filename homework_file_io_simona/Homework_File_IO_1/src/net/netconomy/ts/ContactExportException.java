package net.netconomy.ts;

public class ContactExportException extends ContactBookException{
	public ContactExportException(String errorMessage) {
		super(errorMessage);
	}

}
