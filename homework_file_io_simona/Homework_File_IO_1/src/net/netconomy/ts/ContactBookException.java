package net.netconomy.ts;

public class ContactBookException extends RuntimeException{
		
	public ContactBookException(String errorMessage) {
		super(errorMessage);
	}
}
