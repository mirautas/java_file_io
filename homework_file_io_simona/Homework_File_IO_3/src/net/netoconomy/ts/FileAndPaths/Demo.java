package net.netoconomy.ts.FileAndPaths;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Demo {
	public static void main(String[] args) {
		String sourceDirectory = "C:\\Users\\smirauta\\eclipse-workspace\\Homework_File_IO_3\\sourceFolder";
		Path sourcePath = Paths.get(sourceDirectory); 
		String destinationDirectory = "C:\\\\Users\\\\smirauta\\\\eclipse-workspace\\\\Homework_File_IO_3\\destinationFolder";
		Path destinationPath = Paths.get(destinationDirectory); 
		FileHelper helper = new FileHelper();
		try {
			helper.copyFiles(sourcePath, destinationPath);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
