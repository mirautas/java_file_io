package net.netoconomy.ts.FileAndPaths;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

public class FileHelper {

	public void copyFiles(Path sourcePath, Path destinationPath) throws Exception {
		if (!Files.isDirectory(sourcePath)) {
			throw new Exception(sourcePath.getFileName() + "is not a directory");
		}

		if (!Files.isDirectory(destinationPath)) {
			throw new Exception(destinationPath.getFileName() + "is not a directory");

		}

		File sourceDirectory = sourcePath.toFile();

		String[] files = sourceDirectory.list();
		HashMap<String, Integer> fileNrForExtension = new HashMap<String, Integer>();
		for (String filename : files) {
			String extension = filename.substring(filename.lastIndexOf('.') + 1).trim();
			
			if (fileNrForExtension.containsKey(extension)) {
				fileNrForExtension.put(extension, fileNrForExtension.get(extension) + 1);
			} else {
				fileNrForExtension.put(extension, 1);
			}
			
			System.out.println("Copying file" + filename);
			Files.copy(Paths.get(sourcePath.toString(), filename), Paths.get(destinationPath.toString(), filename),
					StandardCopyOption.REPLACE_EXISTING);
		}
		
		System.out.println("Directory overview:");
		for (String extension: fileNrForExtension.keySet()) {
			System.out.println(extension + " - " + fileNrForExtension.get(extension) + " files");
		}
	}

}
